This is a PANOS configuration lookup tool

--tested on Ubuntu 18.04.2 LTS

--tested on firewalls(not on Panorama) running 8.0, 8.1 PANOS

--single vsys environment

This tool looks up firewall configuration and provide guidance of cleanup

USAGE: 

/panlookup-git$pip3 install -r requirements.txt

/panlookup-git$python3 panlookup.py

MAIN TASKS:

1.  find duplicate address objects and where each is used
2.  find unused address objects and groups
        
3.  find duplicate service objects and where each is used
4.  find unused service objects and groups
        
5.  look up security policies

    (ad hoc cli 'test security-policy-match show-all'
    
    Diff: all fields optional [from/to/src/dst/app/protocol/port])
        
6.  For reference: print selected dictionary

MODULES:

panlookup/menue.py

Task menu

panlookup/session/login.py

Login validation, returns login session info.

panlookup/query/panapi.py

PANOS api queries, return the xml format api response.

panlookup/query/pandict.py

Functions to generate dictionary/list of the objects from corresponding api reply.

panlookup/query/panfind.py

Functions relating to object lookups across multiple dictionaries.

panlookup/file_for_future_use

Folder contains file.py relating to file/data management, not being used in this version

REFERENCE:

For a workflow diagram of all functions, see
https://docs.google.com/drawings/d/1SzUYTMG0p-xRhESipUjroLfDiKMjnNEODjoPIuaCI50

ROADMAP:

In future, below tasks may be added:

* lookup NAT rules
* output to csv format
* routing lookup

