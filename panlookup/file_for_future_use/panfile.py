#!/usr/bin/env python3

#panlookup/file/panfile.py
#save to and load data from file

import shelve
import os
import inspect
import time

def maindir():
    filename = inspect.getframeinfo(inspect.currentframe()).filename
    path     = os.path.dirname(os.path.dirname(os.path.abspath(filename)))

    return path


def savedata(varName,var):

    shelfFile=shelve.open(maindir()+'/data/'+varName)
    shelfFile[varName]=var
    shelfFile.close()
    

def findmtime(varName):
    path=maindir()+'/data/'
    
    if varName in os.listdir(path):
        mTime=time.ctime(os.path.getmtime(path+varName))
        return mTime


def loaddata(varName):
    
    shelfFile=shelve.open(maindir()+'/data/'+varName)
    data=shelfFile[varName]
    shelfFile.close()
    return data
