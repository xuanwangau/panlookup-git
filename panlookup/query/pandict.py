#!/usr/bin/env python3

#panlookup/query/pandict.py
#functions to build object list or dictionary

import ipaddress
import re
from lxml import etree

from . import panapi

#generate address dictionary
def dict_address(dstFw,apiKey):

    root=etree.fromstring(panapi.get_address(dstFw,apiKey))
    elAddress=root[0][0]

    dictAddress={}

    for element in elAddress:
        #get address type
        addrType=element[0].tag

        #get address value based on different types
        if addrType=='fqdn':
            addrValue=element[0].text

        elif addrType=='ip-netmask':
            ipText=element[0].text
            #make /32 a single ip address in a list
            if '/32' in ipText:
                ip=ipaddress.IPv4Network(ipText)[0]
                addrValue=[ip]
            #make other mask length as IPv4Network
            elif '/' in ipText:
                ip=ipaddress.IPv4Network(ipText)
                addrValue=ip
            #put host address in a list
            else:
                ip=ipaddress.IPv4Address(ipText)
                addrValue=[ip]

        else:#addrType=='ip-range'
            #break down to a list of address
            ipRange=element[0].text.split('-')
            ipStart=ipaddress.IPv4Address(ipRange[0])
            ipEnd=ipaddress.IPv4Address(ipRange[1])
            addrValue=[]

            for ip in range(int(ipStart),int(ipEnd)+1):
                addrValue.append(ipaddress.IPv4Address(ip))

        #assign description to des if <description> exists
        elDes=element.find('description')
        if elDes is not None:
            des = elDes.text
        else:
            des = ''

        #assign tag list to variable tag if <tag> exists
        elTag=element.find('tag')
        if elTag is not None:
            tag=[member.text for member in elTag]
        else:
            tag = []

        dictAddress[element.attrib['name']]={
            'type':addrType,
            'value':addrValue,
            'description':des,
            'tag':tag}

    return dictAddress


#build address group dictionary
def dict_addrgroup(dstFw,apiKey):

    root=etree.fromstring(panapi.get_addrgroup(dstFw,apiKey))
    elAddrGroup=root[0][0]

    dictAddrGroup={}

    for element in elAddrGroup:

        groupName=element.attrib['name']

        #assign group type
        groupType=element[0].tag

        #retrieve list of group members
        if groupType == 'static':
            if element[0].find('member') is not None:
                memberList=[member.text for member in element[0]]
            else:
                memberList=[]
        else:#groupType=='dynamic'
            memberList=list_dynamember(dstFw,apiKey,groupName)

        #assign group description
        if element.find('description') is not None:
            des=element.find('description').text
        else:
            des=''

        #assign group tag list
        if element.find('tag') is not None:
            tag=[member.text for member in element.find('tag')]
        else:
            tag=[]

        dictAddrGroup[groupName]={
            'type':groupType,
            'member':memberList,
            'description':des,
            'tag':tag}

    return dictAddrGroup


#find members in a dynamic address group
def list_dynamember(dstFw,apiKey,groupName):

    print('Retrieving members of dynamic group \''+ groupName +'\'...')

    root=etree.fromstring(panapi.get_dynagroup(dstFw,apiKey,groupName))
    elDynaGroup=root[0][0]

    elMember=elDynaGroup[0].find('member-list')
    listDynaMember=[element.attrib['name'] for element in elMember]

    return listDynaMember


#build dictionary for all type of rules in rulebase
def dict_rulebase(dstFw,apiKey):

    root=etree.fromstring(panapi.get_rulebase(dstFw,apiKey))
    elRulebase=root[0][0]

    #list all rule entry as elements
    elRuleList=[el for el in elRulebase.findall('.//entry[@name]')]

    #predefine rule dictionary
    dictRulebase={}

    #nest a dictionary of each rule in dictRulebase
    for elRule in elRuleList:
        ruleName=elRule.attrib['name']
        dictRulebase[ruleName]={}

        #add rule category
        ruleCate=elRule.getparent().getparent().tag
        dictRulebase[ruleName]['rule-category']=ruleCate

        elKeyList=[el for el in elRule]
        for elKey in elKeyList:
            if len(elKey)==0:
                dictRulebase[ruleName][elKey.tag]=elKey.text
            else:
                #get all text in elKey elements
                textList=[el.text for el in elKey.iterdescendants()\
                          if el.text is not None]
                #remove text which only has whitespace
                membList=[x for x in textList if x.strip() != '']
                dictRulebase[ruleName][elKey.tag]=membList

    return dictRulebase

#build dictionary of service objects
def dict_service(dstFw,apiKey):

    rootC=etree.fromstring(panapi.get_service(dstFw,apiKey))
    elCustServ=rootC[0][0]

    rootP=etree.fromstring(panapi.get_predserv(dstFw,apiKey))
    elPredServ=rootP[0]

    #list all service objects as element
    elCustServList=[el for el in elCustServ.findall('.//entry[@name]')]
    elPredServList=[el for el in elPredServ.findall('.//entry[@name]')]

    elServList=elCustServList + elPredServList

    dictService={}

    #build dictionary for each service object
    for elServ in elServList:
        servName=elServ.attrib['name']
        dictService[servName]={}

        elProtocol=elServ.find('protocol')[0]
        protocol=elProtocol.tag
        dictService[servName]['protocol']=protocol

        elPort=elProtocol.find('port')
        port=elPort.text
        dictService[servName]['port']=port

        elTag=elServ.find('tag')
        if elTag!=None:
            tag=[el.text for el in elTag.iterdescendants('member')]
            dictService[servName]['tag']=tag

    return dictService

# build dictionary of service groups
def dict_servgroup(dstFw,apiKey):

    root=etree.fromstring(panapi.get_servgroup(dstFw,apiKey))
    elServGroup=root[0][0]

    elGroupList=[el for el in elServGroup.findall('.//entry[@name]')]

    dictServGroup={}

    for elGroup in elGroupList:
        name=elGroup.attrib['name']
        dictServGroup[name]={}

        elMembers=elGroup.find('members')
        members=[el.text for el in elMembers.iterdescendants('member')\
                 if len(elMembers)!=0]
        dictServGroup[name]['members']=members

        elTag=elGroup.find('tag')
        if elTag!=None:
            tag=[el.text for el in elTag.iterdescendants('member')]
            dictServGroup[name]['tag']=tag

    return dictServGroup

#create predefined application dictionary
def dict_app(dstFw,apiKey):

    rootP=etree.fromstring(panapi.get_predapp(dstFw,apiKey))
    elPredApp=rootP[0]

    rootC=etree.fromstring(panapi.get_custapp(dstFw,apiKey))
    elCustApp=rootC[0][0]

    dictApp={}
    appContainer={}

    elPredList=[el for el in elPredApp.findall('.//entry[@id]')]
    elCustList=[el for el in elCustApp.findall('.//entry[@name]')]

    elAppList=elPredList + elCustList

    for el in elAppList:
        appName=el.attrib['name']
        dictApp[appName]={}

        elDefault=el.find('default')
        if elDefault!=None:
            elProto=elDefault.find('ident-by-ip-protocol')
            if elProto!=None:
                prototext=elProto.text

                dictApp[appName]['ip-protocol']=[prototext]

            elPort=elDefault.find('port')
            if elPort!=None:
                port=[el.text for el in elPort.findall('member')]

                for portEntry in port:
                    splitProto=portEntry.split('/')
                    proto=splitProto[0]
                    numberList=splitProto[1].split(',')
                    dictApp[appName][proto]=numberList

        elContainer=el.find('application-container')
        if elContainer!=None:
            container=elContainer.text
            if container not in appContainer:
                appContainer[container]=[appName]
            else:
                appContainer[container].append(appName)

    return dictApp, appContainer

# build dictionary of application groups
def dict_appgroup(dstFw,apiKey):

    root=etree.fromstring(panapi.get_appgroup(dstFw,apiKey))
    elAppGroup=root[0][0]

    #list app group as elements
    elAppGroupList=[el for el in elAppGroup.findall('.//entry[@name]')]

    dictAppGroup={}

    for elGroup in elAppGroupList:
        groupName=elGroup.attrib['name']
        elMembers=elGroup.find('members')
        if elMembers is not None:
            memberList=[el.text for el in elMembers.iterdescendants('member')]
        dictAppGroup[groupName]=memberList

    return dictAppGroup

# build dictionary of security rules
def dict_secrule(dstFw,apiKey):

    root=etree.fromstring(panapi.get_secrule(dstFw,apiKey))
    elSecRule=root[0][0]

    #list all sec rule as elements
    elRuleList=[el for el in elSecRule.findall('.//entry[@name]')]

    dictSecRule={}

    #build dictionary for each rule
    for elRule in elRuleList:
        ruleName=elRule.attrib['name']
        dictSecRule[ruleName]=parse_secrule(elRule)

    return dictSecRule

#parse security rule from rule elementr
def parse_secrule(elRule):
    parseRule={}
    elKeyList=[el for el in elRule]
    for elKey in elKeyList:
        if len(elKey)==0:
            parseRule[elKey.tag]=elKey.text
        elif elKey.tag=='profile-setting':
            profDict={}
            profEl=[el for el in elKey.iterdescendants('member')]
            for el in profEl:
                profDict[el.getparent().tag]=el.text
            parseRule[elKey.tag]=profDict
        else:
            membList=[el.text for el in elKey.iterdescendants('member')]
            parseRule[elKey.tag]=membList

    return parseRule

#build region dictionary
def dict_region(dstFw,apiKey):
    root=etree.fromstring(panapi.get_region(dstFw,apiKey))
    elRegion=root[0][0]

    #find all entry element of region
    elEntry=[el for el in elRegion.findall('.//entry[@name]')]

    dictRegion={}

    for el in elEntry:
        regionName=el.attrib['name']
        member=[memberEl.text for memberEl in el.iterdescendants('member')]

        memberIP=[]
        for ip in member:
            #only change network members as range might be massively long
            if '/' in ip:
                memberIP.append(ipaddress.IPv4Network(ip))

        dictRegion[regionName]=memberIP

    return dictRegion

#create fqdn dictionary
def dict_fqdn(dstFw,apiKey):
    xml=panapi.get_fqdn(dstFw,apiKey)
    dictFqdn={}

    #retrieve each fqdn entry incl. name and domain
    fqdnReg=re.compile(r'(([\w\-\.]*)\s{2}\(Objectname\s([\w\s\-\.]*)\)\:\s+'
                       '([\d\:a-f\s]+|[\d\.\s\-]+|Not used|Not resolved)+)\s')
    matchList=fqdnReg.findall(xml)
    for match in matchList:
        name=match[2]
        domain=match[1]

        #find ipv4 addresses in each entry
        ipReg=re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
        ipList=ipReg.findall(match[0])

        dictFqdn[name]={
            'domain':domain,
            'ip':ipList }

    return dictFqdn

#get security rule name and index number
def dict_runsec(dstFw,apiKey):
    xml=panapi.get_runsec(dstFw,apiKey)

    #find sections of each running security rule
    #in case of multiple data processors, look for dp0
    dpReg=re.compile(r'DP\ss\ddp\d:(.*)DP\ss\ddp\d:', re.DOTALL)
    dpZero=dpReg.search(xml)

    ruleReg=re.compile(r'"([\w\s\d\-\.]+);\sindex:\s(\d+)"')
    #if dp0 exists (multiple dp)
    if dpZero != None:
        ruleList=ruleReg.findall(dpZero)
    #if dp0 not exist (single dp)
    else:
        ruleList=ruleReg.findall(xml)

    dictRunSec={}
    for rule in ruleList:
        ruleName=rule[0]
        index=rule[1]
        dictRunSec[ruleName]={}
        dictRunSec[ruleName]['index']=index

    return dictRunSec

#build zone dict
def dict_zone(dstFw,apiKey):

    root=etree.fromstring(panapi.get_zone(dstFw,apiKey))
    elZoneRoot=root[0][0]

    #list zone names as elements
    elZoneList=[el for el in elZoneRoot.findall('.//entry[@name]')]

    #predefine zone dict
    dictZone={}

    for elZone in elZoneList:
        zoneName=elZone.attrib['name']
        dictZone[zoneName]={}

        dictZone[zoneName]['member']=[]
        for el in elZone.iterdescendants():
            if el.tag=='member':
                dictZone[zoneName]['member'].append(el.text)

    return dictZone

