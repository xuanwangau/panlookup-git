#!/usr/bin/env python3

#panlookup/query/panapi.py
#pan api calls, return response.text

import requests

#ignore insecure connection warning
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

#get_address api
def get_address(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key':apiKey,
        'xpath':'/config/devices/entry/vsys/entry/address'
        }
    queryUrl = ''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

    
#get_addrgroup api
def get_addrgroup(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key':apiKey,
        'xpath':'/config/devices/entry/vsys/entry/address-group'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text


#get_dynagroup api - show runtime membership of a dynamic address group
def get_dynagroup(dstFw, apiKey, groupName):
    queryParams = {
        'type':'op',
        'cmd':''.join(['<show><object><dynamic-address-group><name>',\
                      groupName,\
                      '</name></dynamic-address-group></object></show>']),
        'key':apiKey
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text


#get_rulebase api
def get_rulebase(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/rulebase'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get predefined services
def get_predserv(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'get',
        'key': apiKey,
        'xpath':'/config/predefined/service/entry'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get service objects
def get_service(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/service'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get service groups
def get_servgroup(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/service-group'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get predefined apps
def get_predapp(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'get',
        'key': apiKey,
        'xpath':'/config/predefined/application/entry'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get app groups
def get_appgroup(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/application-group'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get customized apps
def get_custapp(dstFw,apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/application'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get security rules api
def get_secrule(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/rulebase/security'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get region api
def get_region(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/region'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#request system fqdn
def get_fqdn(dstFw,apiKey):
    queryParams = {
        'type':'op',
        'cmd':'<request><system><fqdn><show/></fqdn></system></request>',
        'key': apiKey
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text

#get running security rules
def get_runsec(dstFw,apiKey):
    queryParams = {
        'type':'op',
        'cmd':'<show><running><security-policy/></running></show>',
        'key': apiKey
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text    

#get zone list api
def get_zone(dstFw, apiKey):
    queryParams = {
        'type':'config',
        'action':'show',
        'key': apiKey,
        'xpath':'/config/devices/entry/vsys/entry/zone'
        }
    queryUrl=''.join(['https://',dstFw,'/api/?'])
    queryResponse = requests.get(queryUrl, params=queryParams, verify=False)

    return queryResponse.text
