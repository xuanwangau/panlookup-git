#!/usr/bin/env python3

#panlookup/query/panfind.py
#search objects in list or dictionary

import ipaddress
import pprint
import copy

from . import pandict


#find duplicate address objects (same value)
def dup_address(dstFw,apiKey):

    dictAddress=pandict.dict_address(dstFw, apiKey)

    #make a flip dictionary using address value as key
    #and append address names which have the same value
    flipDict = {}

    for name,detail in dictAddress.items():
        if str(detail['value']) not in flipDict:
            flipDict[str(detail['value'])]=[name]
        else:
            flipDict[str(detail['value'])].append(name)

    dictAddrGroup=pandict.dict_addrgroup(dstFw,apiKey)
    dictRulebase =pandict.dict_rulebase(dstFw,apiKey)
    
    dupAddress = {}
    #dup addresses are the ones in flipDict which has more than one name
    for valueStr, nameList in flipDict.items():
        if len(nameList)>1:
            dupAddress[valueStr]={}
            #find where the dup addresses are used
            for name in nameList:
                
                dupAddress[valueStr][name]=[]
                
                #look for address name in address groups
                for group in dictAddrGroup.keys():
                    if name in dictAddrGroup[group]['member']:
                        dupAddress[valueStr][name].append(\
                            ('Address Group',group))

                #look for address name in rules
                for rule in dictRulebase.keys():
                    for ruleKey in dictRulebase[rule].keys():
                        if name in dictRulebase[rule][ruleKey]:
                            ruleCate=dictRulebase[rule]['rule-category']
                            dupAddress[valueStr][name].append(\
                                (ruleCate,rule))

    return dupAddress

#find unused address and address groups
''' Address object or group is considered "unused" if:
    It's not a member of any (other) address group, and
    it's not appearing in any rule set - rulebase section.
'''
   
def unused_address(dstFw,apiKey):

    #load required dictionary
    dictAddress=pandict.dict_address(dstFw,apiKey)
    
    dictAddrGroup=pandict.dict_addrgroup(dstFw,apiKey)

    dictRulebase=pandict.dict_rulebase(dstFw,apiKey)

    #filter out unused address from dictAddress
    unusedAddress = {} 
        
    for address in dictAddress.keys():
        #look in address group
        inGroup=in_addrgroup(address,dictAddrGroup)
        
        #look in rules
        inRule=in_rule(address,dictRulebase)
        
        if not(inGroup or inRule):
            unusedAddress[address] = dictAddress[address]

    #filter out unused address group from dictAddrGroup
    unusedAddrGroup = {}
    
    for group in dictAddrGroup.keys():
        #look in address group
        inGroup=in_addrgroup(group,dictAddrGroup)
        
        #look in rules
        inRule=in_rule(group,dictRulebase)
        
        if not(inGroup or inRule):
            unusedAddrGroup[group] = dictAddrGroup[group]

    return unusedAddress,unusedAddrGroup

#check if object in address group
def in_addrgroup(obj, dictAddrGroup):
    inGroup=False
    for group, groupDetail in dictAddrGroup.items():
            if obj in groupDetail['member']:
                inGroup=True
                return inGroup

#check if object is used in rule
def in_rule(obj,dictRulebase):
    inRule=False
    for rule, ruleDetail in dictRulebase.items():
        for ruleKey, keyDetail in ruleDetail.items():
            if obj in keyDetail:
                inRule=True
                return inRule

#find duplicate service objects (same value)
def dup_service(dstFw,apiKey):

    dictService=pandict.dict_service(dstFw, apiKey)

    #make a flip dictionary using service value as key
    #and append service names which have the same value
    flipDict = {}

    for name,detail in dictService.items():
        protoPort=detail['protocol'] + ': ' + detail['port']
        if protoPort not in flipDict:
            flipDict[protoPort]=[name]
        else:
            flipDict[protoPort].append(name)

    dictServGroup=pandict.dict_servgroup(dstFw,apiKey)
    dictRulebase =pandict.dict_rulebase(dstFw,apiKey)
    
    dupService = {}
    #dup service are the ones in flipDict which has more than one name
    for valueStr, nameList in flipDict.items():
        if len(nameList)>1:
            dupService[valueStr]={}
            #find where the dup services are used
            for name in nameList:
                
                dupService[valueStr][name]=[]
                
                #look for service name in service groups
                for group in dictServGroup.keys():
                    if name in dictServGroup[group]['members']:
                        dupService[valueStr][name].append(\
                            ('Service Group',group))

                #look for service name in rules
                for rule in dictRulebase.keys():
                    for ruleKey in dictRulebase[rule].keys():
                        if name in dictRulebase[rule][ruleKey]:
                            ruleCate=dictRulebase[rule]['rule-category']
                            dupService[valueStr][name].append(\
                                (ruleCate,rule))

    return dupService

#find unused services and service groups
''' Service object or group is considered "unused" if:
    It's not a member of any (other) service group, and
    it's not appearing in any rule set - rulebase section.
'''
   
def unused_service(dstFw,apiKey):

    #load required dictionary
    dictService=pandict.dict_service(dstFw,apiKey)
    
    dictServGroup=pandict.dict_servgroup(dstFw,apiKey)

    dictRulebase=pandict.dict_rulebase(dstFw,apiKey)

    #filter out unused service from dictService
    unusedService = {} 
        
    for service in dictService.keys():
        #look in service group
        inGroup=in_servgroup(service,dictServGroup)
        
        #look in rules
        inRule=in_rule(service,dictRulebase)
        
        if not(inGroup or inRule):
            unusedService[service] = dictService[service]

    #filter out unused service group from dictServGroup
    unusedServGroup = {}
    
    for group in dictServGroup.keys():
        #look in service group
        inGroup=in_servgroup(group,dictServGroup)
        
        #look in rules
        inRule=in_rule(group,dictRulebase)
        
        if not(inGroup or inRule):
            unusedServGroup[group] = dictServGroup[group]

    return unusedService,unusedServGroup

#check if object in service group
def in_servgroup(obj, dictServGroup):
    inGroup=False
    for group, groupDetail in dictServGroup.items():
            if obj in groupDetail['members']:
                inGroup=True
                return inGroup

#break down ip address list from source or destination
def breakdown_address(addressKey,dictRegion,dictAddress,dictFqdn,dictAddrGroup):

	ipList=[]
	groupMember=[]
	while len(addressKey)!=0:
		for member in addressKey:
			if member=='any':
				ipList.append(ipaddress.IPv4Network('0.0.0.0/0'))
			elif member in dictRegion:
				for net in dictRegion[member]:
					ipList.append(net)
			elif member in dictAddress:
				if dictAddress[member]['type']=='fqdn':
					for ip in dictFqdn[member]['ip']:
						ipList.append([ipaddress.IPv4Address(ip)])
				else:
					ipList.append(dictAddress[member]['value'])
			elif member in dictAddrGroup:
				groupMember=dictAddrGroup[member]['member']
				for address in groupMember:
					addressKey.append(address)
			else:
				if '/' in member:
					ipList.append(ipaddress.IPv4Network(member))
				else:
					ip=ipaddress.IPv4Address(member)
					ipList.append([ip])
					
			addressKey.remove(member)

	return ipList
	
#break down service (protocol:ports)
def breakdown_service(serviceList,dictService,dictServGroup):
    
    grouped={}
    
    while len(serviceList)!=0:
        for service in serviceList:
            if service in ['any','application-default']:
                grouped['any']=service
                
            elif service in dictService:
                proto=dictService[service]['protocol']
                port=dictService[service]['port']
                if proto not in grouped:
                    grouped[proto]=[port]
                elif port not in grouped[proto]:
                    grouped[proto].append(port)
            elif service in dictServGroup:
                groupMember=dictServGroup[service]['members']
                for member in groupMember:
                    serviceList.append(member)
                    
            serviceList.remove(service)

    return grouped

#breakdown applications
def breakdown_app(appList,appContainer,dictAppGroup):
    
        appBreakdown=[]

        if 'any' in appList:
            appBreakdown.append('any')
        else:
            while len(appList)!=0:
                for app in appList:
                    if app in dictAppGroup:
                        for member in dictAppGroup[app]:
                            appList.append(member)
                    elif app in appContainer:
                        for member in appContainer[app]:
                            appList.append(member)
                    elif app not in appBreakdown:
                        appBreakdown.append(app)
                        
                    appList.remove(app)

        return appBreakdown

#dissect security rule details
def detail_secrule(dstFw,apiKey):
    print('\nStart building security rule dictionary...')
    
    #prepare all dictionaries required
    
    #basic security rule
    dictSecRule=pandict.dict_secrule(dstFw,apiKey)

    #dictionaries below parse source/destination in rule
    print('working on address objects and groups...')
    dictAddress=pandict.dict_address(dstFw,apiKey)
    dictAddrGroup=pandict.dict_addrgroup(dstFw,apiKey)
    dictRegion=pandict.dict_region(dstFw,apiKey)
    dictFqdn=pandict.dict_fqdn(dstFw,apiKey)

    #dictionaries below parse service in rule
    print('working on service objects and groups...')
    dictService=pandict.dict_service(dstFw,apiKey)
    dictServGroup=pandict.dict_servgroup(dstFw,apiKey)

    #dictionaries below parse application in rule
    print('working on applications and groups...')
    dictApp,appContainer=pandict.dict_app(dstFw,apiKey)
    dictAppGroup=pandict.dict_appgroup(dstFw,apiKey)

    for rule, detail in dictSecRule.items():
        #break down source and destination to ip address list
        srcIP=detail['source']
        detail['source']=breakdown_address(srcIP,dictRegion,dictAddress,\
                                           dictFqdn,dictAddrGroup)
        dstIP=detail['destination']
        detail['destination']=breakdown_address(dstIP,dictRegion,dictAddress,\
                                           dictFqdn,dictAddrGroup)

        #break down service to protocol and port
        serviceList=detail['service']
        detail['service']=breakdown_service(serviceList,dictService,dictServGroup)
        
        #break down application to app list
        appList=detail['application']
        detail['application']=breakdown_app(appList,appContainer,dictAppGroup)

        #update service based on applications
        apps=detail['application']
        servs=detail['service']

        if 'any' not in apps and 'any' in servs:
            if servs['any']=='application-default':
                for app in apps:
                    for protocol, ports in dictApp[app].items():
                        if protocol not in servs:
                            servs[protocol]=copy.deepcopy(dictApp[app][protocol])
                        else:
                            for port in ports:
                                if port not in servs[protocol]:
                                    servs[protocol].append(port)

    print('Complete building security rule dictionary...')
    
    return dictSecRule

#test security policy match
def matching_rule(dstFw,apiKey):
    
    matchCriteria=match_criteria(dstFw,apiKey)
    
    #prepare dictionaries for rule matching
    detailSecRule=detail_secrule(dstFw,apiKey)
    dictRunSec=pandict.dict_runsec(dstFw,apiKey)
    
    matchingRule={}

    for rule, detail in detailSecRule.items():
        count=[]

        #check from/to zones in rule
        if matchCriteria['from']=='' \
           or 'any' in detail['from'] \
           or matchCriteria['from'] in detail['from']:
            count.append(True)
        if matchCriteria['to']=='' \
           or 'any' in detail['to'] \
           or matchCriteria['to'] in detail['to']:
            count.append(True)

        #check source/destination address in rules         
        if matchCriteria['source']=='':
            count.append(True)
        else:
            for src in detail['source']:
                if matchCriteria['source'] in src:
                    count.append(True)
                    break
        if matchCriteria['destination']=='':
            count.append(True)
        else:
            for dst in detail['destination']:
                if matchCriteria['destination'] in dst:
                    count.append(True)
                    break
                
        #check application in rule
        if matchCriteria['application']=='':
            count.append(True)
        elif 'any' in detail['application']:
            count.append(True)
        else:
            for app in detail['application']:
                if matchCriteria['application'] in app:
                    count.append(True)
                    break

        #check service (protocol+port) in rule
        protocol=matchCriteria['protocol']
        port=matchCriteria['port']
        servs=detail['service']

        if protocol=='any':
            if 'any' in servs and servs['any']=='any':
                count.append(True)
        elif protocol=='':
            if port=='':
                count.append(True)
            elif 'any' in detail['application']:
                if 'any' in servs:
                    count.append(True)
            else:
                for proto in servs:
                    if port_in_proto(proto,port,servs):
                        count.append(True)
                        break
        else:#protocol in ['tcp','udp','ip']
            if port=='':
                if protocol in servs:
                    count.append(True)
            elif 'any' in detail['application']:
                if 'any' in servs and servs['any']=='application-default':
                    count.append(True)
            else:
                count.append(port_in_proto(protocol,port,servs))

        if sum(count)==6:
            matchingRule[rule]=detail

    for rule in matchingRule:
        matchingRule[rule]['index']=dictRunSec[rule]['index']

    return matchingRule
#port in service regardless protocol
def port_in_proto(protocol,port,servs):
    if protocol in servs and protocol != 'any':
        for number in servs[protocol]:
            if '-' in number:
                start=number.split('-')[0]
                end=number.split('-')[1]
                if int(port)>=int(start) and int(port)<=int(end):
                    return True
            elif ',' in number:
                numList=number.split(',')
                if port in numList:
                    return True
            elif port==number:
                return True
    return False
            
#input and validate match criteria
def match_criteria(dstFw,apiKey):
    print('\nProvide criteria to search in security rules...')
    print('''
Note:
        -------------------------------------------------------------
            Type in 'any' will literally match 'any' in policies;
            Or just Enter to ignore (match all rules).
        -------------------------------------------------------------
            ''')

    dictZone=pandict.dict_zone(dstFw,apiKey)
            
    matchList={}

    #validate 'from' zone
    while True:
        fromZone=input('Source (from) zone:')
        if fromZone in ['','any'] or fromZone in dictZone:
            matchList['from']=fromZone
            break
        else:
            guess=[zone for zone in dictZone.keys() \
                   if fromZone.lower() in str(zone).lower()]
            if len(guess)==0:
                print('Could not find zone name \''+fromZone+'\'')
            else:
                print('Invalid zone name, did you mean: (case sensitive)')
                for zone in guess:
                    print(zone)
                                  
    #validate 'to' zone
    while True:
        toZone=input('Destination (to) zone:')
        if toZone in ['','any'] or toZone in dictZone:
            matchList['to']=toZone
            break
        else:
            guess=[zone for zone in dictZone.keys() \
                   if toZone.lower() in str(zone).lower()]
            if len(guess)==0:
                print('Could not find zone name \''+toZone+'\'')
            else:
                print('Invalid zone name, did you mean: (case sensitive)')
                for zone in guess:
                    print(zone)
                      
    #validate source ip
    while True:
        src=input('Source ip:')
        if src=='':
            matchList['source']=''
            break
        elif src=='any':
            matchList['source']=ipaddress.IPv4Address('0.0.0.0')
            break
        else:
            try:
                matchList['source']=ipaddress.IPv4Address(src)
                break
            except ipaddress.AddressValueError:
                print('Invalid ip address')
        
    #validate destination ip
    while True:
        dst=input('Destination ip:')
        if dst=='':
            matchList['destination']=''
            break
        elif dst=='any':
            matchList['destination']=ipaddress.IPv4Address('0.0.0.0')
            break
        else:
            try:
                matchList['destination']=ipaddress.IPv4Address(dst)
                break
            except ipaddress.AddressValueError:
                print('Invalid ip address')

    #input application
    app=input('Application name (or partial):')
    matchList['application']=app.lower()
    if app not in ['','any']:
        print('(Providing protocol and/or port is recommended\n'
              'along with specified application.)')

    #validate protocol
    while True:
        protocol=input('Service/protocol(any|tcp|udp|ip):')
        if protocol not in ['any','tcp','udp','ip','']:
            print('Invalid protocol.')
        elif protocol=='any':
            matchList['protocol']='any'
            print('Port is omitted for service "any".')
            matchList['port']=''
            break
        elif protocol=='ip':
            matchList['protocol']='ip-protocol'
            break
        else:
            matchList['protocol']=protocol
            break
    #validate port or ip protocol number
    while matchList['protocol']!='any':
        port=input('Port number or ip-protocol number:')
        if port == '':
            matchList['port']=''
            break
        else:
            try:
                matchList['port']=str(int(port))
                break
            except ValueError:
                print('Invalid number.')

    print('\nProvided matching criteria are:')
    print('Source zone'.ljust(30,'.')+\
          matchList['from'].rjust(10))
    print('Destination zone'.ljust(30,'.')+\
          matchList['to'].rjust(10))
    print('Source IP'.ljust(30,'.')+\
          str(matchList['source']).rjust(10))
    print('Destination IP'.ljust(30,'.')+\
          str(matchList['destination']).rjust(10))
    print('Application'.ljust(30,'.')+\
          matchList['application'].rjust(10))
    print('Protocol'.ljust(30,'.')+\
          matchList['protocol'].rjust(10))
    print('Port or IP protocol Number'.ljust(30,'.')+\
          matchList['port'].rjust(10))
    input('Press ENTER to continue...')

    return matchList
