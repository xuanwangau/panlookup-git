#!/usr/bin/env python3

# panlookup/session/login.py
    

""" Login procedure
    Validate user login and return session data
"""

import requests
import re
from getpass import getpass

#ignore insecure connection warning
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

#user login and validation, max attempts 3, create session data if succeed

def create_session():
    session = {'dstFw':'',
               'adminName':'',
               'adminPass':'',
               'apiKey':''
               }

    loginCount = 0

    while True:
        #ask user destination, admin name and password
        
        print('Enter the firewall ip address you want to manage:')
        session['dstFw'] = input()

        print ('Username: ', end='')
        session['adminName'] = input()

        session['adminPass'] = getpass('Password:')
        
        #validate login using api keygen request
        keyGenUrl= ''.join(['https://',session['dstFw'],'/api/?type=keygen&user=',\
                   session['adminName'],'&password=',session['adminPass']])
        keyGen=''
        
        try:
            keyGen = requests.get(keyGenUrl,verify=False,timeout=2)
        except requests.exceptions.InvalidURL:
            print('---Invalid destination.---')
        except:
            print('---Connection timeout.---')
        
        if '<Response [200]>' in str(keyGen):
                        
            #look for api key from keyGen reply
            keyRegex = re.compile(r'<key>(.*)<\/key>')
            key = keyRegex.search(keyGen.text)

            #if key found, return session info
            #otherwise wrong destination
            if key:
                session['apiKey']=key.group(1)
                print('Connected to firewall:' + session['dstFw']\
                  + ' by:' + session['adminName'])
                break
            else:
                print('Unexpected response from destination: ' +\
                      session['dstFw'])
                loginCount += 1
                max_out(loginCount)
                
        #login forbidden by destination
        elif '<Response [403]>' in str(keyGen):
            print('Invalid username or password.')
            loginCount += 1
            max_out(loginCount)
            
        #no proper response received
        else:
            print('Unexpected response from destination: ' +\
                      session['dstFw'])
            loginCount += 1
            max_out(loginCount)
                
    return session
        
def max_out(attempts):
    if attempts == 3:
        print('\nMax attempts reached. Task aborted.')
        sys.exit()
