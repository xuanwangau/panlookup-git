#!/usr/bin/env python3

# panlookup/panlookup.py
    
print('Running panlookup...')

import sys
import pprint

#login procedure
from .session import login

session = login.create_session()

#present tasks menu
from .query import panfind, pandict

dstFw=session['dstFw']
apiKey=session['apiKey']

while True:
    
    print('''
        Choose from following tasks:\n
        1.  find duplicate address objects and where each is used
        2.  find unused address objects and groups
        
        3.  find duplicate service objects and where each is used
        4.  find unused service objects and groups
        
        5.  look up security policies
            (ad hoc cli 'test security-policy-match show-all'
            Diff: all fields optional [from/to/src/dst/app/protocol/port])
        
        6.  For reference: print selected dictionary
        ''')
    
    #perform tasks in menu based on user choice
    task=input('Enter number to do the task or any other key to quit:')
            
    if task=='1':
        #generate dictionary of duplicate address
        dupAddress=panfind.dup_address(dstFw,apiKey)

        #print dup addresses and origins
        print('\nFound '+str(len(dupAddress))+\
              ' sets of duplicate addresses...')
        input('Press ENTER to continue...')
        
        for k,v in dupAddress.items():
            print('\nAddresses ' + str(list(v.keys())) + \
                  ' have identical value:' + '\n' + k.rjust(80,'.'))
            print('\nwhere,',end='')
            for name in v.keys():
                if len(v[name])!=0:
                    print('\n\''+ name +'\' is used in:')
                    for tup in v[name]:
                        print(tup[0] +': ' + tup[1])
                else:
                    print('\n\''+ name + '\' is not in use.')
                    
        print('\nEnd of ' + str(len(dupAddress)) + ' duplicates.')
        

    elif task=='2':
        unusedAddress,unusedAddrGroup=panfind.unused_address(dstFw,apiKey)
        
        #print unused addresses
        print('\nFound '+ str(len(unusedAddress)) +' unused address objects.')

        input("Press ENTER to display...")
        for n in unusedAddress:
            print(n.ljust(30, '.') + unusedAddress[n]['type'].rjust(60))
        
        #print unused address groups
        print('\nFound '+ str(len(unusedAddrGroup)) +' unused address groups.')

        input("Press ENTER to display...")
        for n in unusedAddrGroup:
            print(n.ljust(30, '.') + unusedAddrGroup[n]['type'].rjust(60))

    elif task=='3':
        #generate dictionary of duplicate service
        dupService=panfind.dup_service(dstFw,apiKey)

        #print dup services and origins
        print('\nFound '+str(len(dupService))+\
              ' sets of duplicate service objects...')
        input('Press ENTER to continue...')
        
        for k,v in dupService.items():
            print('\nServices ' + str(list(v.keys())) + \
                  ' have identical value:' + '\n' + k.rjust(80,'.'))
            print('\nwhere,',end='')
            for name in v.keys():
                if len(v[name])!=0:
                    print('\n\''+ name +'\' is used in:')
                    for tup in v[name]:
                        print(tup[0] +': ' + tup[1])
                else:
                    print('\n\''+ name + '\' is not in use.')
                    
        print('\nEnd of ' + str(len(dupService)) + ' duplicates.')
        
    elif task=='4':
        unusedService,unusedServGroup=panfind.unused_service(dstFw,apiKey)
        
        #print unused services
        print('\nFound '+ str(len(unusedService)) +' unused service objects.')

        input("Press ENTER to display...")
        for n in unusedService:
            protoPort=unusedService[n]['protocol'] + ': ' + \
                       unusedService[n]['port']
            print(n.ljust(30, '.') + protoPort.rjust(60))
        
        #print unused service groups
        print('\nFound '+ str(len(unusedServGroup)) +' unused service groups.')

        input("Press ENTER to display...")
        for n in unusedServGroup:
            print(n)

    elif task=='5':
        while True:
            matchingRule=panfind.matching_rule(dstFw,apiKey)
            
            if len(matchingRule)>30:
                print('\nToo many ('+str(len(matchingRule))+') matching rules found.')
                print('Consider refine search criteria.')
            else:
                print('\nFound '+str(len(matchingRule))+' matching rules.')
                while len(matchingRule)!=0:
                    input('Press Enter to list matching rules:')
                    print('Rule Name'.ljust(40,'.') + 'Index'.rjust(40))
                    for name in matchingRule:
                        index=matchingRule[name]['index']
                        print(name.ljust(40,'.')+ index.rjust(40))
                    choice=input('Type index number to display rule detail,'
                                 'or just enter to skip:')
                    if choice=='':
                        break
                    else:
                        indexList=[matchingRule[name]['index'] for name in matchingRule]
                        if choice not in indexList:
                            print('Index '+ choice + ' not found in matching rules.\n')
                        else:
                            for name,detail in matchingRule.items():
                                if detail['index']==choice:
                                    print('\nRule name: '+name)
                                    pprint.pprint(detail)
                        
            again=input('Enter "y" to search again, or any other key to end task:')
            if again.lower()!='y':
                break
            
    elif task=='6':
        collection ={
            'address':pandict.dict_address(dstFw,apiKey),
            'address-group':pandict.dict_addrgroup(dstFw,apiKey),
            'service':pandict.dict_service(dstFw,apiKey),
            'service-group':pandict.dict_servgroup(dstFw,apiKey),
            'application':pandict.dict_app(dstFw,apiKey)[0],
            'app-group':pandict.dict_appgroup(dstFw,apiKey),
            'security-rule':panfind.detail_secrule(dstFw,apiKey),
            'region':pandict.dict_region(dstFw,apiKey),
            'fqdn':pandict.dict_fqdn(dstFw,apiKey)
            }
        while True:
            dictName=input('\nType dictionary names to display'
                           '\nor just Enter to end task:'
                           '\naddress'
                           '\naddress-group'
                           '\nservice'
                           '\nservice-group'
                           '\napplication'
                           '\napp-group'
                           '\nsecurity-rule'
                           '\nregion'
                           '\nfqdn'
                           '\n:')
            if dictName in ['address','address-group','service','service-group',\
                            'application','app-group','security-rule',\
                            'region','fqdn']:
                pprint.pprint(collection[dictName])
            elif dictName =='':
                break
            else:
                print('Invalid dictionary name.')
        
    else:
        sys.exit()

    print('\nEnter "Q" to quit or any other key to return to menu...')
    
    if input().lower()=='q':
        sys.exit()
